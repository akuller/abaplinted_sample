# Example of automatic abaplint execution via gitlab CI for ABAP project

An example to my SCN post ["Automatic checking of your ABAP code in Github/Gitlab with CI and abaplint"](https://blogs.sap.com/2018/12/25/automatic-checking-of-your-abap-code-in-githubgitlab-with-ci-and-abaplint
)

- See "Commit failed" mark at the top (click it)
- See `.gitlab-ci.yml` as an example config

## Workflow to choose

There are 2 workflows in the example, you may choose one of them (just copy pase the appropriate one):
- `run_abaplint_docker` - runs via official abaplint docker image
- `run_abaplint` - runs via nodejs npm

Both are valid, choose one that fits better to your environment.

## Older versions

Original code for the post moved to [version-2018-12](https://gitlab.com/atsybulsky/abaplinted_sample/-/tree/version-2018-12) branch. Current version uses the updated `abaplint` installation procedure and new version of config.
